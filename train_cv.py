import numpy as np
import os
from itertools import product, izip
from rtrbm import train_rtrbm
#from joblib import Parallel, delayed
from sklearn.utils import check_arrays
import sys
from pydub import AudioSegment
import scipy.io.wavfile as wav
from scipy.signal import resample
from scikits.talkbox.features import mfcc
from matplotlib import pyplot as plt
from matplotlib.mlab import PCA
from utils import *

"""Example code for performing classification with the Theano DIRTRBM class.

The data is containd in a shelve file, whose dictionary has the following 
fields:
    X           : Input sequences
    y           : Target sequences
    fold_labels : Fold labels for all the data-points
"""

def train_and_evaluate(hyper_param, X, fold_labels, rng):
    """Do a fold-wise evaluation of one model in the grid search.

    Input
    -----
    hyper_param: dictionary : Hyperparameters (as keys) and their values.
    X: np.ndarray(float) : Input data
    y: np.ndarray(float) : Class labels
    fold_labels: np.ndarray(int) : Fold labels
    rng: np.random.rng : Random number generator


    Output
    ------
    grid_point: dictionary: Model details, and error-rates
    """
    grid_point = {}
    grid_point['models'] = []
    grid_point['hypers'] = hyper_param
    grid_point['scores'] = []

    n_folds = np.unique(fold_labels).shape[0]

    for f in xrange(n_folds):
        print "\nFold %d" % (f+1)
        # Prepare training, validation and test data for this fold
        n_seqs = len(X)
        X_trainvalid = [X[seq_idx] for seq_idx in xrange(n_seqs)
                        if fold_labels[seq_idx] != f]
        X_test = [X[seq_idx] for seq_idx in xrange(n_seqs)
                  if fold_labels[seq_idx] == f]
        #y_trainvalid = [y[seq_idx] for seq_idx in xrange(n_seqs)
                        #if fold_labels[seq_idx] != f]
        #y_test = [y[seq_idx] for seq_idx in xrange(n_seqs)
                  #if fold_labels[seq_idx] == f]
        
        n_trainvalid = len(X_trainvalid)
        n_valid = np.max((np.int32(np.ceil(n_trainvalid/20)), 5))
        n_train = n_trainvalid - n_valid
        
        idxs = rng.permutation(n_trainvalid)
        X_train = [X_trainvalid[idx] for idx in idxs[:n_train]]
        X_valid = [X_trainvalid[idx] for idx in idxs[n_train:]]
        #y_train = [y_trainvalid[idx] for idx in idxs[:n_train]]
        #y_valid = [y_trainvalid[idx] for idx in idxs[n_train:]]
         
        dataset = ((X_train), (X_valid), (X_test))

        # Train model
        model, scores = train_rtrbm(n_hidden=hyper_param['n_hiddens'],
                                      learning_rate=hyper_param['learning_rate'],
                                      weight_decay=hyper_param['weight_decay'],
                                      num_epochs=hyper_param['n_epoch'], 
                                      batch_size=hyper_param['batch_size'],
                                      dataset=dataset) 
        grid_point['models'].append(model)
        grid_point['scores'].append(scores)

    return grid_point


def grid_search(hyper_params, X, fold_labels, rng):
    """Search a grid of hyperparameters for the best model on given data.

    Input
    -----
    hyper_params: dictionary : Hyperparameters (as keys) and their values.
    X: np.ndarray(float) : Input data
    y: np.ndarray(float) : Class labels
    fold_labels: np.ndarray(int) : Fold labels
    rng: np.random.rng : Random number generator

    Output
    ------
    model_grid: list : Details about the evaluated models and their error rates
    """
    n_folds = np.unique(fold_labels).shape[0]

    def my_product(dicts):
        return (dict(izip(dicts, x)) for x in product(*dicts.itervalues()))

    hyper_param_grid = my_product(hyper_params)

    # Data array check 
    # XXX: This is only needed if using sklearn to parallelize, so if someone
    # doesn't have sklearn installed, and are sure that the data types of the
    # arrays are ok (see next two lines), then these lines can be commented.
    #X = [check_arrays(X_el, dtype=np.float)[0] for X_el in X]
    #y = [check_arrays(y_el, dtype=np.int)[0] for y_el in y]
  
    # Perform grid search in parallel using joblib
    # XXX: I've had "lock" problems while using joblib and Theano together,
    # which I haven't resolved yet, so I'm using whatever parallelization
    # Numpy can manage on the CPU with OpenBLAS.
#    model_grid = Parallel(n_jobs=4)(delayed(train_and_evaluate)(hyper_param, X,
#                                                                y, fold_labels, 
#                                                                rng)
#                                   for hyper_param in hyper_param_grid)
    model_grid = [train_and_evaluate(hyper_param, X, fold_labels, rng) 
                  for hyper_param in hyper_param_grid]
    
    print "Done with grid search."
    return model_grid


if __name__ == "__main__":
    # List of datasets
    # XXX: Results will be saved to the same folders as the datasets.
    dataset_file = os.path.abspath(sys.argv[1])
    currentPath = os.path.dirname(os.path.realpath(__file__))
    dataset_wavfile = os.path.join(currentPath, "wav_cache/" + os.path.basename(dataset_file).split('.')[0] + ".wav")

    extension = os.path.splitext(dataset_file)[1]

    if extension == ",wav" or extension == ".mp3":
        print "Audio file detected\n"
        if not os.path.isdir(os.path.dirname(dataset_wavfile)):
            os.mkdir(os.path.dirname(dataset_wavfile))
            
        if extension != ".wav":
            print "Attempting to convert to wav format\n"

        if extension != ".wav" and not os.path.isfile(dataset_wavfile):
            print "Converting %s to wav format" % (os.path.basename(dataset_file))
            sound = AudioSegment.from_mp3(dataset_file)
            sound = sound.set_channels(1)
            sound.export(dataset_wavfile, format="wav")
            print "Saved file to %s" % (dataset_wavfile)
        else:
            print "%s already exists in %s so using that instead\n" % (os.path.basename(dataset_wavfile), os.path.dirname(dataset_wavfile))

        if extension == ".wav" and not os.path.isfile(dataset_wavfile):
            sound = AudioSegment.from_wav(dataset_file)
            if sound.channels > 1:
                sound = sound.set_channels(1)

            sound.export(dataset_wavfile, format="wav")
            print "Saved file to %s" % (dataset_wavfile)

    dataset_file = dataset_wavfile

    rng = np.random.RandomState(1234)

    #t = np.arange(0,1000,1)
    #f = np.array([100])

    #X = np.array([np.sin(math.pi*t*f[i]*0.001) for i in xrange(len(f))])

    rate, data = wav.read(dataset_file)
    num_samples = 10000
    window_size = 200
    sliding_window_size = window_size/2
    window_size_secs = int((float(window_size) / float(num_samples)) * 1000)
    print "Resampling %s to %s samples" % (os.path.basename(dataset_file), num_samples)
    resampled_data = resample(data, num_samples)

    print "Applying FFT on subsequences with a windows size of %s samples,\nwhich corresponds to about %s ms" % (window_size, window_size_secs)
    X = []
    windowed = sliding_window(data, window_size, sliding_window_size)
    window = np.hanning(window_size)
    for i in xrange(len(windowed)/num_samples):
        X.append(np.abs(np.fft.fft(windowed[i*num_samples:(i+1)*num_samples]*window, axis=0)))

    num_folds = 5
    print "Generating random fold labels of %s folds" % (num_folds)
    fold_labels = np.random.randint(0, num_folds, len(X))

    # Define hyperparameter grid
    hyper_params = {'n_hiddens': [100],
                    'learning_rate': [0.001],
                    'weight_decay': [0.0000],
                    'batch_size': [500],
                    'n_epoch': [250]}
                    #'softmax_pattern': [data['softmax_config']]}

    model_grid = grid_search(hyper_params, X, fold_labels, rng)

    # Save model grid
    #slv = shelve.open(out_path+'/model-grid.slv')
    #slv['model_grid'] = model_grid
    #slv.close()

    print "End of program"
