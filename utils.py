"""Various utility functions.

cross_entropy:
  Compute cross-entropy of the probability distribution predicted by a model,
  given the target values for certain input.

index_to_onehot:
  Convert a sequence of integer indices into a one-hot vector sequence.

onehot_to_index:
  Convert a sequence of one-hot vectors into a sequence of integer indices.
"""


import numpy as np
from numpy.lib.stride_tricks import as_strided as ast

def cross_entropy(probs, tgts):
    """
    Compute cross-entropy of the probability distribution predicted by a model,
    given the target values for certain input.

    Input:
    ------
    probs: The predicted distributions for each sample of input data.
    tgts: Target values corresponding to each input.

    Output:
    -------
    cr_en: Cross entropy.
    """
    cr_en = -np.mean(np.log2(probs)[np.arange(tgts.shape[0]), tgts])

    return cr_en


def index_to_onehot(idx_seq, n_class):
    """
    Convert a sequence of integer indices into a one-hot vector sequence.

    Input:
    ------
    idx_seq: Sequence of integer indices(positive, and smallest index as 1).
    n_class: Length of one-hot vector.

    Output:
    -------
    onehot_seq: A numpy array of size n_class x m, where m is the length of
                 the sequence.

    Note that this function only works for a single sequence. If multiple
    sequences are input simultaneously, as in multiple viewpoint sequences, it
    won't do the conversion correctly. There may even be an error. I haven't
    checked. I'll do it later if required.
    """
    n_pts, = np.shape(idx_seq)
    onehot_seq = np.zeros((n_pts, n_class))
    idx = np.argwhere(idx_seq > n_class)
    
    if idx.size > 0:
        print "Invalid class label. Returning zero-array."
        return onehot_seq

    for i in range(n_pts):
        onehot_seq[i, idx_seq[i]] = 1

    return onehot_seq


def onehot_to_index(onehot_seq):
    """
    Convert a sequence of one-hot vectors into a sequence of integer indices.
    """
    return np.where(onehot_seq)[1]


def truncate_sequences(x, tr_len):
    """Split sequences into subsequences of fixed length.

    Input
    -----
    x: list(np.ndarray)
      Input sequences
    y: list(np.ndarray)
      Label sequences
    tr_len: int
      Truncation length

    Output
    ------
    x_tr: list(np.ndarray)
      Truncated input sequences
    y_tr: list(np.ndarray)
      Truncated label sequences
    """
    n_sequences = len(x) # y should also have the same length
    n_channels = x.shape[1]

    x_tr = []
    #y_tr = []
    for c in xtange(n_channels):
        for s in xrange(n_sequences):
            in_seq = x[s,c]
            #lab_seq = y[s]
            len_seq = in_seq.shape[0]
            l = 0
            while l < len_seq:
                x_tr.append(in_seq[:, l:np.min((l+tr_len, len_seq))])
                #y_tr.append(lab_seq[l:np.min((l+tr_len, len_seq))])
                l += tr_len

    return x_tr#, y_tr

def partition(seq, chunks):
    """Splits the sequence into equal sized chunks and outputs them as a list"""
    result = []
    for i in range(chunks):
        chunk = []
        for element in seq[i:len(seq):chunks]:
            chunk.append(np.array(element))
        result.append(np.array(chunk))
    return result

def norm_shape(shape):
    '''
    Normalize numpy array shapes so they're always expressed as a tuple, 
    even for one-dimensional shapes.
     
    Parameters
        shape - an int, or a tuple of ints
     
    Returns
        a shape tuple
    '''
    try:
        i = int(shape)
        return (i,)
    except TypeError:
        # shape was not a number
        pass
 
    try:
        t = tuple(shape)
        return t
    except TypeError:
        # shape was not iterable
        pass
     
    raise TypeError('shape must be an int, or a tuple of ints')

def sliding_window(a,ws,ss = None,flatten = True):
    '''
    Return a sliding window over a in any number of dimensions
     
    Parameters:
        a  - an n-dimensional numpy array
        ws - an int (a is 1D) or tuple (a is 2D or greater) representing the size 
             of each dimension of the window
        ss - an int (a is 1D) or tuple (a is 2D or greater) representing the 
             amount to slide the window in each dimension. If not specified, it
             defaults to ws.
        flatten - if True, all slices are flattened, otherwise, there is an 
                  extra dimension for each dimension of the input.
     
    Returns
        an array containing each n-dimensional window from a
    '''
     
    if None is ss:
        # ss was not provided. the windows will not overlap in any direction.
        ss = ws
    ws = norm_shape(ws)
    ss = norm_shape(ss)
     
    # convert ws, ss, and a.shape to numpy arrays so that we can do math in every 
    # dimension at once.
    ws = np.array(ws)
    ss = np.array(ss)
    shape = np.array(a.shape)
     
     
    # ensure that ws, ss, and a.shape all have the same number of dimensions
    ls = [len(shape),len(ws),len(ss)]
    if 1 != len(set(ls)):
        raise ValueError(\
        'a.shape, ws and ss must all have the same length. They were %s' % str(ls))
     
    # ensure that ws is smaller than a in every dimension
    if np.any(ws > shape):
        raise ValueError(\
        'ws cannot be larger than a in any dimension.\
 a.shape was %s and ws was %s' % (str(a.shape),str(ws)))
     
    # how many slices will there be in each dimension?
    newshape = norm_shape(((shape - ws) // ss) + 1)
    # the shape of the strided array will be the number of slices in each dimension
    # plus the shape of the window (tuple addition)
    newshape += norm_shape(ws)
    # the strides tuple will be the array's strides multiplied by step size, plus
    # the array's strides (tuple addition)
    newstrides = norm_shape(np.array(a.strides) * ss) + a.strides
    strided = ast(a,shape = newshape,strides = newstrides)
    if not flatten:
        return strided
     
    # Collapse strided so that it has one more dimension than the window.  I.e.,
    # the new array is a flat list of slices.
    meat = len(ws) if ws.shape else 0
    firstdim = (np.product(newshape[:-meat]),) if ws.shape else ()
    dim = firstdim + (newshape[-meat:])
    # remove any dimensions with size 1
    dim = filter(lambda i : i != 1,dim)
    return strided.reshape(dim)
