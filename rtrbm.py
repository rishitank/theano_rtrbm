import numpy as np
from rbm import pretrain_rbm
import theano
import theano.tensor as T
import time
from utils import *

# XXX: The first random number generator is only for sampling from the visible
# layer of the RBM in the model, and the second is for everything else.
t_rng = T.shared_randomstreams.RandomStreams(860331)
n_rng = np.random.RandomState(860331)

# theano.config.warn.subtensor_merge_bug = False # XXX: What is this for?
theano.config.exception_verbosity = 'high'


"""Implementation of the Discriminative Inference Recurrent Temporal Restricted
Boltzmann Machine which is trained using backpropagation through time and
stochastic gradient descent."""


def build_rbm(v, n_input, n_class, W, bv, bh, k):
    """Construct a k-step Gibbs chain starting at v for an RBM.

    Input
    -----
    v : Theano vector or matrix
      If a matrix, multiple chains will be run in parallel (batch).
    n_input : integer
      Dimensionality of input feature.
    n_class : integer
      Number of output classes.
    W : Theano matrix
      Weight matrix of the RBM.
    bv : Theano vector
      Visible bias vector of the RBM.
    bh : Theano vector
      Hidden bias vector of the RBM.
    k : scalar or Theano scalar
      Length of the Gibbs chain.
    
    Output
    ------
    Return a (v_sample, cost, monitor, updates) tuple containing...
    
    v_sample : Theano vector or matrix with the same shape as `v`
      Corresponds to the generated sample(s).
    cost : Theano scalar
      Expression whose gradient with respect to W, bv, bh is the CD-k
      approximation to the log-likelihood of `v` (training example) under the
      RBM. The cost is averaged in the batch case.
    monitor: Theano scalar
      Pseudo log-likelihood (also averaged in the batch case).
    updates: dictionary of Theano variable -> Theano variable
      The `updates` object returned by scan."""

    # One iteration of the Gibbs sampler.
    # XXX: At the moment, this can do sampling for stochastic units and
    # equal-sized sets of softmax units. However, for the most general case I
    # may want to try later, varying sizes of softmax unit sets are required.
    # TODO: Implement sampling from visible layer with different sizes of
    # softmax units.
    def gibbs_step(v):
        # Compute visible layer activations given hidden layer
        mean_h = T.nnet.sigmoid(T.dot(v, W) + bh)
        h = t_rng.binomial(size=mean_h.shape, n=1, p=mean_h,
                           dtype=theano.config.floatX)
        acts_v = T.dot(h, W.T) + bv

        # Multinomial visible units sampling (equally sized)
        #acts_in = acts_v[:, :n_input]
        #probs_in = T.nnet.softmax(acts_in)
        #v_in = t_rng.multinomial(n=1, pvals=probs_in, 
                                 #dtype=theano.config.floatX)
        #acts_out = acts_v[:, -n_class:]
        #probs_out = T.nnet.softmax(acts_out)
        #v_out = t_rng.multinomial(n=1, pvals=probs_out, 
                                  #dtype=theano.config.floatX)
        #mean_v = T.concatenate((probs_in, probs_out), axis=1)
        #v = T.concatenate((v_in, v_out), axis=1)

        # Gaussian visible units
        mean_v = T.dot(h, W.T) + bv
        v = mean_v + t_rng.normal(size=mean_v.shape, avg=0.0, std=1.0, dtype=theano.config.floatX)

        # Binomial visible units sampling
#        mean_v = T.nnet.sigmoid(acts_v)
#        v = t_rng.binomial(size=mean_v.shape, n=1, p=mean_v,
#                           dtype=theano.config.floatX)
        
        return mean_v, v

    # Gibbs sampling loop
    chain, updates = theano.scan(lambda v: gibbs_step(v)[1],
                                 outputs_info=[v], non_sequences=[],
                                 n_steps=k) 
    v_sample = chain[-1]
    mean_v = gibbs_step(v_sample)[0]

    # Pseudo-likelihood computation to monitor training progress
    #monitor = T.xlogx.xlogy0(v, mean_v) + T.xlogx.xlogy0(1-v, 1-mean_v)
    #monitor = monitor.sum() / v.shape[0]

    # L2 Error
    monitor = (v-mean_v)**2

    def free_energy(v):
        """Free energy of RBM visible layer."""
        return -(v * bv).sum() - T.log(1 + T.exp(T.dot(v, W) + bh)).sum() + 0.5*(v**2).sum()
    cost = (free_energy(v) - free_energy(v_sample)) / v.shape[0]

    return v_sample, cost, monitor, updates


def shared_normal(num_rows, num_cols, scale=1):
    """Initialize a matrix shared variable with normally distributed
    elements."""
    return theano.shared(n_rng.normal(
        scale=scale, size=(num_rows, num_cols)).astype(theano.config.floatX))


def shared_zeros(*shape):
    """Initialize a vector shared variable with zero elements."""
    return theano.shared(np.zeros(shape, dtype=theano.config.floatX))


def build_rtrbm(n_visible, n_hidden, n_class, W_init=None, Whh_init=None,
                  Whv_init=None, bv_init=None, bh_init=None, 
                  L1_decay=0.0, L2_decay= 0.0):
    """Function to build the Theano graph for the RTRBM.

    Input
    -----
    n_visible : integer
      Number of visible units.
    n_hidden : integer
      Number of hidden units of the conditional RBMs.
    n_class : integer
      Number of classification categories.
    W_init : np.ndarray(float)
      Weight matrix between h(t) and v(t).
    Whh_init : np.ndarray(float)
      Weight matrix between h(t-1) and h(t).
    Whv_init : np.ndarray(float)
      Weight matrix between h(t-1) and v(t).
    bv_init : np.ndarray(float)
      (Time-independent) visible bias vector.
    bh_init : np.ndarray(float)
      (Time-independent) hidden bias vector.
    L1_decay : float
      L1-regularization (weight decay) hyperparameter.
    L2_decay : float
      L2-regularization (weight decay) hyperparameter.
   
    Output
    ------
    v : Theano matrix
      Symbolic variable holding an input sequence (used during training)
    v_sample : Theano matrix
      Symbolic variable holding the negative particles for CD log-likelihood
      gradient estimation (used during training)
    cost : Theano scalar
      Expression whose gradient (considering v_sample constant) corresponds to
      the LL gradient of the RTRBM (used during training)
    monitor : Theano scalar
      Frame-level pseudo-likelihood (useful for monitoring during training)
    params : tuple of Theano shared variables
      The parameters of the model to be optimized during training.
    updates_train : dictionary of Theano variable -> Theano variable
      Update object that should be passed to theano.function when compiling the
      training function.
    v_t : Theano matrix
      Symbolic variable holding a generated sequence (used during sampling)
    """
    # Initialize inputs and initial hidden layer activations.
    v = T.matrix()  # a training sequence
    h0 = T.zeros((n_hidden,))

    # Initialize model parameters
    if W_init is None:
        W = shared_normal(n_visible, n_hidden, 0.01)
    else:
        W = theano.shared(W_init)
    if bv_init is None:
        bv = shared_zeros(n_visible)
    else:
        bv = theano.shared(bv_init)
    if bh_init is None:
        bh = shared_zeros(n_hidden)
    else:
        bh = theano.shared(bh_init)
    if Whh_init is None:
        Whh = shared_normal(n_hidden, n_hidden, 0.0001)
    else:
        Whh = theano.shared(Whh_init)
    if Whv_init is None:
        Whv = shared_normal(n_hidden, n_visible, 0.0001)
    else:
        Whv = theano.shared(Whv_init)

    params = W, bv, bh, Whv, Whh

    # Model recurrence
    def recurrence(v_t, h_tm1):
        bv_t = bv + T.dot(h_tm1, Whv)
        bh_t = bh + T.dot(h_tm1, Whh)
        h_t = T.nnet.sigmoid(bh + T.dot(v_t, W) + T.dot(h_tm1, Whh))
        return [h_t, bv_t, bh_t]

    # XXX: Conditional RBMs can be trained in batches using the below idea. For
    # training, the deterministic recurrence is used to compute all the {bv_t,
    # bh_t, 1 <= t <= T} given v. So here, the (time-dependent) dynamic biases
    # are pre-computed using scan, and are batch-processed in build_rbm. This
    # is something I may be able to exploit even during prediction, since I'm
    # forward-propagating the one-hot class vectors anyway.
    (h_t, bv_t, bh_t), updates_train = theano.scan(
            lambda v_t, h_tm1, *_: recurrence(v_t, h_tm1), 
            sequences=v, outputs_info=[h0, None, None], non_sequences=params)
    n_input = n_visible-n_class
    v_sample, cost, monitor, updates_rbm = build_rbm(v, n_input, n_class, W,
                                                     bv_t[:], bh_t[:], k=15) 
    updates_train.update(updates_rbm)

    # Add weight decay (regularization) to cost.
    L1 = abs(W).sum()
    L2_sqr = (W**2).sum()
    cost += (L1_decay*L1 + L2_decay*L2_sqr)

    # Prediction at each time-instant
    Y_class = T.eye(n_class, dtype=theano.config.floatX)
    def p_y_given_x(v_t, h_tm1):
        U = W[-n_class:, :] # or, U = W[n_input:, :]
        V = W[:-n_class, :] # or, V = W[:n_input, :]
        c = bh
        d = bv[-n_class:] # or, d = bv[:n_input]
        
        x_t = v_t[:-n_class]
        c_t = c + T.dot(h_tm1, Whh)
        d_t = d + T.dot(h_tm1, Whv[:, -n_class:])

        s_hid = theano.tensor.dot(x_t, V) + c_t
        energies, updates = theano.scan(lambda y_class, U, s_hid: 
                                        s_hid + theano.tensor.dot(y_class, U),
                                        sequences=[Y_class],
                                        non_sequences=[U, s_hid])
        log_p, updates = theano.scan(
                lambda d_i, e_i: d_i + \
                        theano.tensor.sum(theano.tensor.log(1+theano.tensor.exp(e_i))),
                        sequences=[d_t, energies], non_sequences=[])
        
        p_t = T.nnet.softmax(log_p.T) # XXX: Can the transpose be avoided?
        h_t = T.nnet.sigmoid(T.dot(v_t, W) + bh + T.dot(h_tm1, Whh))

        return [p_t, h_t]

    # Sequential prediction loop.
    (p_y_given_x, h_t), _ = theano.scan(
            lambda v_t, h_tm1, *_: p_y_given_x(v_t, h_tm1),
            sequences=v, 
            outputs_info=[None, h0], 
            non_sequences=params)
    p_y_given_x = p_y_given_x[:, 0, :]
    y_out = T.argmax(p_y_given_x, axis=-1)
    
    return (v, v_sample, cost, monitor, params, updates_train, p_y_given_x,
            y_out) 


class RTRBM:
    """Discriminative Inference RTRBM class"""

    def __init__(self, n_input, n_class, n_hidden=150, learning_rate=0.001,
                 L1_reg=0.0000, L2_reg=0.0000, n_epoch=100, batch_size=500,
                 W_init=None, Whh_init=None, Whv_init=None, bv_init=None,
                 bh_init=None):
        """Constructs and compiles Theano functions for training and
        prediction.

        Input
        -----
        n_input : integer
          Number of inputs which make up a part of the visible layer.
        n_class : integer
          Number of output classes which make up the rest of the visible layer.
        n_hidden : integer
          Number of hidden units of the conditional RBMs.
        learning_rate : float
          Learning rate
        L1_reg : float
          L1 weight decay.
        L2_reg : float
          L2 weight decay.
        n_epoch : integer
          Number of training epochs.
        batch_size : integer
          Maximum sequence length for truncated BPTT.
        W_init : numpy.ndarray
          Initial values of weights h(t) and v(t)
        Whh_init : numpy.ndarray
          Initial values of weights between h(t-1) and h(t)
        Whv_init : numpy.ndarray
          Initial values of weights between h(t-1) and v(t)
        bv_init : numpy.ndarray
          Initial values of visible biases
        bh_init : numpy.ndarray
          Initial values of hidden biases
        """
        self.n_input = n_input
        self.n_class = n_class
        self.n_visible = n_input
        self.n_hidden = n_hidden
        self.n_epoch = n_epoch
        self.learning_rate = learning_rate
        self.L1 = L1_reg
        self.L2_sqr = L2_reg
        self.batch_size = batch_size

        # Build the model graph.
        (v, v_sample, cost, monitor, params, updates_train, p_y_given_x, y_out) = \
                build_rtrbm(n_input, n_hidden, n_class, W_init,
                        Whh_init, Whv_init, bv_init, bh_init, L1_reg, L2_reg)

        # Compute parameter gradients w.r.t cost.
        gradients = T.grad(cost, params, consider_constant=[v_sample])
        updates_train.update(((param, param - learning_rate * gradient) 
                              for param, gradient in zip(params, gradients)))
        
        # Functions for training, evaluating and saving the model.
        self.train_function = theano.function([v], cost,
                                              updates=updates_train,
                                              allow_input_downcast=True)
        self.predict_proba = theano.function([v], p_y_given_x,
                                             allow_input_downcast=True)
        self.predict_function = theano.function([v], y_out, 
                                                allow_input_downcast=True)
        self.get_model_parameters = theano.function([], params)


    def train(self, dataset, learning_rate=0.01, batch_size=100,
              num_epochs=200): 
        """Train the RTRBM via stochastic gradient descent (SGD). 

        Input
        -----
        dataset : tuple of three tuples of two numpy.ndarrays 
          Training, validation and test sequences.

        Output
        ------
        best_model_parameters: list(numpy.ndarray)
          A list containing the model parameters (see class definition).
        best_validation_loss: float
          Validation set cross entropy.
        """
        # Load training and validation data
        X_train = dataset[0]
        X_valid = dataset[1]
        
        n_train = len(X_train)
        n_valid = len(X_valid)
    
        # Combine inputs and one-hot targets into visible layer of the model
        #Y_train = [index_to_onehot(seq, self.n_class) for seq in y_train]
        #Y_valid = [index_to_onehot(seq, self.n_class) for seq in y_valid]
        #XY_train = [np.concatenate((X_train[seq_idx].T, Y_train[seq_idx]),
                                   #axis=1) for seq_idx in xrange(n_train)]
        #XY_valid = [np.concatenate((X_valid[seq_idx].T, Y_valid[seq_idx]),
                                   #axis=1) for seq_idx in xrange(n_valid)]

        # Train the model
        print 'Training model...'
        validation_frequency = 1
        best_valid_score = np.inf
        best_model_parameters = None
        start_time = time.clock()
        
        # TODO: Implement early stopping later
        learning_rate = self.learning_rate
        for epoch in xrange(self.n_epoch): 
            n_rng.shuffle(X_train)
            costs = []
            for sequence in X_train:
                len_seq = sequence.shape[0]
                for i in xrange(0, len_seq, self.batch_size):
                    cost = self.train_function(sequence[i:i+self.batch_size, :])
                    costs.append(cost)
            mean_train_cost = np.mean(costs)
            print('Epoch %i/%i, train loss: %.3f' % 
                  (epoch+1, self.n_epoch, mean_train_cost)) 

            #if (epoch + 1) % validation_frequency == 0:
                ## Compute validation cross entropy
                #valid_pred = []
                #for i in xrange(n_valid):
                    #valid_pred.append(self.predict_proba(X_valid[i]))

                #this_valid_score = np.mean(cross_entropy(
                        #np.concatenate(tuple(valid_pred), axis=0),
                        #np.concatenate(tuple(y_valid), axis=0)))
                #print("\tValidation loss: %.3f (best: %.3f)" % 
                      #(this_valid_score, best_valid_score))

                #if this_valid_score < best_valid_score:
                    #best_valid_score = this_valid_score
                    #best_model_parameters = self.get_model_parameters()

            #learning_rate *= self.rate_decay
        end_time= time.clock()
        print('\nTime taken to train model for %d epochs: %.3f\n' %
              (self.n_epoch, end_time-start_time))

        return best_model_parameters#, best_valid_score


def train_rtrbm(n_hidden=100, learning_rate=0.01, weight_decay=0.0000, 
                  num_epochs=100, batch_size=100, dataset=None):
    """Stochastic gradient descent optimization of a RTRBM.

    Input
    -----
    n_hiddens: int 
      Number of hidden units
    learning_rate: float 
      Learning rate
    weight_decay: float
      Weight decay (regularization) hyperparameter
    num_epochs: integer 
      Number of epochs for training
    batch_size: int 
      Batch size for batch/mini-batch gradient descent
    dataset: tuple(tuple(np.ndarray)) 
      Training and validation sets

    Output
    ------
    params: list(np.ndarray)
      List of all the model parameters (see class definition)
    valid_ce: float
      Validation set cross entropy
    test_ce: float
      Test set cross entropy
    test_acc: float
      Test set prediction accuracy
    """
    # Read training, validation and test sets
    X_train = dataset[0]
    X_valid = dataset[1]
    X_test = dataset[2]

    # Truncate sequences to a specified maximum length and update dataset
    #X_train = truncate_sequences(X_train, batch_size) 
    #dataset = ((X_train), (X_valid), (X_test))

    # Obtain the number of training, validation and test samples
    n_train = len(X_train)
    n_valid = len(X_valid)
    n_test = len(X_test)
   
    # We need these as well, and note that n_visible = n_input + n_class
    n_input = X_valid[0].shape[0]
    n_class = n_input#np.max(np.concatenate(tuple(X_train+X_test+X_valid))) + 1

    # Pre-train an RBM (this does help quite a lot for most of the datasets)
    #Y_train = [index_to_onehot(seq, n_class) for seq in y_train]
    #XY_train = tuple(
        #[np.concatenate((X_train[seq_idx].T), axis=1)
        #for seq_idx in xrange(n_train)])
    dataset_pretrain = np.concatenate(X_train, axis=0)

    print "Pretraining RBM model"
    rbm_model = pretrain_rbm(dataset=dataset_pretrain,
                             n_visible=200, n_hidden=n_hidden,
                             learning_rate=0.001, training_epochs=5,
                             batch_size=100) 

    print "Initialising RTRBM model, given pretrained model parameters"
    # Train a RTRBM with pre-trained RBM weights and biases as initial values
    model = RTRBM(n_input=200, n_class=n_class, n_hidden=n_hidden,
                    learning_rate=learning_rate, L1_reg=weight_decay,
                    L2_reg=weight_decay, n_epoch=num_epochs,
                    W_init=rbm_model.W.eval(), bv_init=rbm_model.vbias.eval(),
                    bh_init=rbm_model.hbias.eval()) 
    params, valid_ce = model.train(dataset, batch_size=batch_size,
                                   num_epochs=num_epochs)  
    
    # Evaluate the trained model on test data
    print "Testing the model..."
    model = RTRBM(n_input=200, n_class=n_class, n_hidden=n_hidden,
                    learning_rate=learning_rate, L1_reg=weight_decay,
                    L2_reg=weight_decay, n_epoch=num_epochs, W_init=params[0],
                    Whh_init=params[4], Whv_init=params[3], bv_init=params[1],
                    bh_init=params[2])
    #Y_test = [index_to_onehot(seq, n_class) for seq in y_test]
    #XY_test = tuple([np.concatenate((X_test[seq_idx].T, Y_test[seq_idx]),
                                     #axis=1) for seq_idx in xrange(n_test)])
    
    # Cross entropy
    y_prob = []
    for i in xrange(n_test):
        y_prob.append(model.predict_proba(XY_test[i]))
    test_ce = np.mean(cross_entropy(
            np.concatenate(tuple(y_prob), axis=0),
            np.concatenate(tuple(y_test), axis=0)))

    # Accuracy
    y_pred = []
    for i in xrange(n_test):
        y_pred.append(model.predict_function(XY_test[i]))
    test_acc = np.float(np.sum(np.concatenate(tuple(y_pred), axis=0) == 
                               np.concatenate(tuple(y_test), axis=0))) / \
            np.shape(np.concatenate(tuple(y_pred), axis=0))[0]
    

    print("Test cross entropy: %.3f\n" % (test_ce))
    print("Test accuracy: %.3f\n" % (test_acc))

    return params, (valid_ce, test_ce, test_acc)


if __name__ == '__main__':
    print "Did not implement a main() function. Use with train_cv.py."
